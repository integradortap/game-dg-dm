extends Node2D

onready var hud = $HUD
onready var screen = $Screen

const levelBasePath = "res://scenes/levels/Level%s.tscn"

var loadedLevel
var defaultLevel = 1
var max_level = 2
var currentLevel = defaultLevel

func _ready():
	load_level(defaultLevel)
	
func load_next_level():
	if(screen != null):
		screen.queue_free()
	self.remove_child(screen)
	currentLevel += 1
	load_level(currentLevel)
	
func reload_level():
	load_level(currentLevel)

func load_level(level):
	if(level > max_level):
		level = defaultLevel
	var levelPath = levelBasePath % str(level)
	var levelScene = load(levelPath).instance()
	self.add_child(levelScene)
	self.move_child(levelScene,0)
	self.remove_child(loadedLevel)
	levelScene.set_name("Screen")
	screen = levelScene
	loadedLevel = screen
	currentLevel = level
	hud.set_counter(level)
