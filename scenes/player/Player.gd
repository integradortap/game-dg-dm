extends KinematicBody2D

onready var spriteMovement = $AnimatedSprite
onready var game = get_node("/root/Game")

var vel: Vector2
var speed = 50
var dead = false
var withReward = false

func _ready():
	pass

func _process(delta):
	_handlePlayerState()
	_handleMovement(delta)
	
func _handlePlayerState():
	if dead:
		if Input.is_action_just_pressed("ui_page_down"):
			show()
			$CollisionShape2D.disabled = false
			dead = false
		return
	
func _handleMovementCommands():
	var newVel = Vector2()
	if Input.is_action_pressed("ui_down"):
		newVel.y = speed
	elif Input.is_action_pressed("ui_up"):
		newVel.y = -speed
	elif Input.is_action_pressed("ui_right"):
		newVel.x = speed
		spriteMovement.set_flip_h(false)
	elif Input.is_action_pressed("ui_left"):
		newVel.x = -speed
		spriteMovement.set_flip_h(true)
	return newVel
	
func _handleMovement(delta):
	vel = _handleMovementCommands()
	if vel.length() > 0:
		spriteMovement.play('running')
		var info = move_and_collide(vel * delta)
		_handleCollision(info)
	else:
		spriteMovement.play('idle')
			
func kill():
		_setWithReward(false)
		hide()
		$CollisionShape2D.disabled = true
		game.reload_level()
		dead = true

func _handleCollision(info):
	if info:
		if 'Zombie' in info.get_collider().name:
			kill()
		if 'Reward' in info.get_collider().name:
			_setWithReward(true)
			info.get_collider().reward()
		if 'Exit'in info.get_collider().name:
			if (withReward):
				game.load_next_level()
				
func _setWithReward(reward):
	withReward = reward
	game.hud.showReward(withReward)
