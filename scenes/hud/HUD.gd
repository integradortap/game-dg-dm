extends CanvasLayer

func _ready():
	$RewardSprite.visible = false

func set_counter(counter):
	$Counter.text = str(counter)
	
func showReward(show):
	$RewardSprite.visible = show
