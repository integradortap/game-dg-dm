extends KinematicBody2D

var globalDelta

onready var player = get_parent().get_node("Player")
onready var spriteMovement = $AnimatedSprite

func _ready():
	randomize()

#Child needs to implement
func new_goal():
	pass
	
func _process(delta):
	globalDelta = delta
	new_goal()
