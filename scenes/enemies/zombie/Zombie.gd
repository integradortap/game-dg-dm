extends "res://scenes/enemies/Enemy.gd"

var goal: String
var speed = 45

##move random
const DOWN = 0
const RIGHT = 1
const UP = 2
const LEFT = 3
 
var direction: int = randi() % 4
var steps = 50 + (randi() % 100)
var countSteps = 0
##

func new_goal():
	if(player.vel.length() > 0):
		goal = "Follow"
		_followPlayer()
	else:
		goal = "Random"
		_walkRandomly()

func _followPlayer():
	var player_direction = (player.global_position - global_position).normalized()
	_setFlip(player_direction)
	_handleColision(move_and_collide(player_direction * speed * globalDelta))

func _setFlip(dir: Vector2):
	if(dir.x > 0):
		spriteMovement.set_flip_h(false)
	else:
		spriteMovement.set_flip_h(true)
	
func _newWalkRandomly():
	direction = randi() % 4
	steps = 50 + (randi() % 100)
	countSteps = 0
	
func _walkRandomly():
	countSteps += 1
	if countSteps > steps:
		_newWalkRandomly()
	_handleRandomMovement()
		
func _handleRandomMovement():
	var vel = Vector2()
	if direction == DOWN:
		vel.y = speed;
	elif direction == RIGHT:
		vel.x = speed
		spriteMovement.set_flip_h(false)
	elif direction == UP:
		vel.y = -speed
	elif direction == LEFT:
		vel.x = -speed
		spriteMovement.set_flip_h(true)
	if vel.length() > 0:
		_handleColision(move_and_collide(vel * globalDelta))
		spriteMovement.play('running')
	else:
		spriteMovement.play('idle')

func _handleColision(info):
	if info:
		if not ('Player' in info.get_collider().name):
			if goal == "Random":
				_newWalkRandomly()
		else:
			info.get_collider().kill()
